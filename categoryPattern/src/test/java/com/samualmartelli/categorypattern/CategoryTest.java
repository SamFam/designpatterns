/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.categorypattern;

import java.util.HashSet;
import java.util.Set;
import junit.framework.Assert;
import static junit.framework.Assert.assertTrue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author hard-
 */
public class CategoryTest {

    private Category testCategoryOne;
    private Category testCategoryTwo;
    private SubCategory testSubCategoryOne;
    private SubCategory testSubCategoryTwo;
    
    private CategoryManager testManager;
    
    private Set<String> actors;
    private Set<String> musicians;
    private Set<String> artists;
    private Set<String> bands;
    
    private void setUpTestSets() {
        actors = new HashSet<>();
        actors.add("Manu Bennett");
        actors.add("Jessica Alba");
        actors.add("Charlize Theron");
        musicians = new HashSet<>();
        musicians.add("Red Hot Chili Peppers");
        musicians.add("Eminem");
        musicians.add("Barenaked Ladies");
    }
    
    private void setUpTestSubSets() {
        artists = new HashSet<>(); 
        bands = new HashSet<>(); 
        artists.add("Eminem");
        bands.add("Red Hot Chili Peppers");
        bands.add("Barenaked Ladies");
    }
    
    @Before
    public void setUp(){
        setUpTestSets();
        testCategoryOne = new Category(actors, "Actors");
        testCategoryTwo = new Category(musicians, "Musicians");
        
        testManager = new CategoryManager();
        testCategoryOne.setService(testManager);
        testCategoryTwo.setService(testManager);
    }
    
    @After
    public void tearDown(){
        
    }
    
    @Test
    public void testAddAndRemove () {
        final Integer newActorCount = actors.size() + 1;
        testCategoryOne.addToList("Dwayne Johnson");
        System.out.println("Actor Count: " + newActorCount);
        assertTrue(testCategoryOne.getCategoryList().size() == newActorCount);
        testCategoryOne.removeFromList("Dwayne Johnson");
        assertTrue(testCategoryOne.getCategoryList().size() == actors.size());
    }
    
    @Test
    public void testSimplifyCategories () {
        Category simplified = testCategoryOne.createSimplifiedCategory(actors, musicians, "Famous People");
        assertTrue(simplified.getCategoryList().size() == (actors.size() + musicians.size()));
        System.out.println(simplified.toString());
    }
    
    @Test
    public void testCollapseCategories () {
        Category simplified = testCategoryOne.collapseCategory(testCategoryOne, testCategoryTwo, "Famous People");
        assertTrue(simplified.getCategoryList().size() == (actors.size() + musicians.size()));
        System.out.println(simplified.toString());
    }
    
    @Test
    public void testSubCategories () {
        setUpTestSubSets();
        
        SubCategory artistSubCategory = new SubCategory(artists, "Artists", testCategoryTwo);
        artistSubCategory.setService(testManager);
        
        SubCategory bandSubCategory = (SubCategory) artistSubCategory.createSubCategory(testCategoryTwo, bands, "Bands");
        assertTrue(bandSubCategory.getCategoryList().size() == bands.size());
        assertTrue(bandSubCategory.getParentCategoryList().size() == musicians.size());
    }
}
