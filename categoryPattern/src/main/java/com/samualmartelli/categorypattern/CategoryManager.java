/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.categorypattern;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The CategoryManager provides services for Categories
 * @author Samual Martelli
 */
public class CategoryManager implements CategoryService {

    @Override
    public Category createSimplifiedCategory(Set<String> listOne, Set<String> listTwo, String simplifiedName) {
        Set<String> simplifiedList = new HashSet<String>();
        simplifiedList.addAll(listOne);
        simplifiedList.addAll(listTwo);
        return new Category(simplifiedList, simplifiedName);
    }

    @Override
    public Category collapseCategory(Category one, Category two, String name) {
        Set<String> simplifiedList = new HashSet<String>();
        simplifiedList.addAll(one.getCategoryList());
        simplifiedList.addAll(two.getCategoryList());
        return new Category(simplifiedList, name);
    }

    @Override
    public Category createSubCategory(Category parent, Set<String> list, String name) {
        return new SubCategory(list, name, parent);
    }
    
}
