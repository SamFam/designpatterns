/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.categorypattern;

import java.util.Set;

/**
 * This interface provides all services to the Category object
 * @author Samual Martlli
 */
public interface CategoryService {
    
    /**
     * Creates a simplified category from two separate lists
     * @param listOne
     * @param listTwo
     * @return
     */
    public Category createSimplifiedCategory(Set<String> listOne, Set<String> listTwo, String simplifiedName);
    
    /**
     * Creates a collapsed category from two categories;
     * @param listOne
     * @param listTwo
     * @return
     */
    public Category collapseCategory(Category one, Category two, String name);
    
    /**
     * Creates a SubCategory of its parent Category;
     * @param parent The parent Category
     * @param list The list of elements in the subcategory
     * @param name The name of the subcategory
     * @return
     */
    public Category createSubCategory(Category parent, Set<String> list, String name);
}
