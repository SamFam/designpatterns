/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.categorypattern;

import java.util.Set;
import org.springframework.stereotype.Component;

/**
 * This represents a SubCategory for a given Category.
 * @author Samual Martelli
 */
@Component
public class SubCategory extends Category {
    
    private final Category parent;
  
    public SubCategory(Set<String> list, String name, Category parent) {
        super(list, name);
        this.parent = parent;
    }
    
    public Category getParent() {
        return parent;
    }
    
    public Set<String> getParentCategoryList() {
        return parent.getCategoryList();
    }
    
    public Category createSubCategory (Category parent, Set<String> list, String name) {
        return this.service.createSubCategory(parent, list, name);
    }
}
