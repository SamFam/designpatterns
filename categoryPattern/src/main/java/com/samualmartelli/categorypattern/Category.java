/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.categorypattern;

import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class represents a category of similar objects
 *
 * @author Samual Martelli
 */
@Component
public class Category {

    @Autowired
    protected CategoryService service;

    protected Set<String> list;
    
    protected final String name;

    public Category(Set<String> existingList, String categoryName) {
        list = existingList;
        name = categoryName;
    }

    public void addToList(String item) {
        list.add(item);
    }

    public void removeFromList(String item) {
        list.remove(item);
    }

    public Set<String> getCategoryList() {
        return list;
    }

    public String getName() {
        return name;
    }
    
    @Autowired
    public void setService(CategoryService service) {
        this.service = service;
    }
    
    @Override
    public String toString() {
        return "Category - " + name + ":\n" + list.toString();
    }
    
    public Category createSimplifiedCategory(Set<String> listOne, Set<String> listTwo, String name) {
        return this.service.createSimplifiedCategory(listOne, listTwo, name);
    }

    public Category collapseCategory (Category one, Category two, String name) {
        return this.service.collapseCategory(one, two, name);
    }
    
}
