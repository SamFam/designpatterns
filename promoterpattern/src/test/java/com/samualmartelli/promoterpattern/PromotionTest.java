/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.promoterpattern;

import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import threats.*;

/**
 *This class tests the promotion of a Target to a threat
 * @author Samual Martelli
 */
public class PromotionTest {

    private static final Integer EMPTY_SIZE = 0;
    private Tracker testTracker = new Tracker();
    private Position testPosition;
    private Target testTarget;

    @Before
    public void setUp() {
        testPosition = new Position(1, 1, 1);
        testTarget = new Target(testPosition, false);
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testPromote() {
        testTracker.addTarget(testTarget);
        assertTrue(testTracker.isHostile(testTarget));
        assertTrue(testTracker.getTargets().size() > EMPTY_SIZE);
        assertTrue(testTracker.getThreats().size() == EMPTY_SIZE);
        testTracker.checkForPromotion(testTarget);
        assertTrue(testTracker.getThreats().size() > EMPTY_SIZE);
        assertTrue(testTracker.getTargets().size() == EMPTY_SIZE);
    }
}
