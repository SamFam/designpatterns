/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.promoterpattern;

import com.samualmartelli.promoterpattern.interfaces.Promotable;
import com.samualmartelli.promoterpattern.interfaces.PromotionException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import threats.Threat;

/**
 * This class tests the Threat object 
 * @author Samual Martelli
 */
public class ThreatTest {
    Promotable mockSourceObject = mock(Promotable.class);
    
    @Before
    public void setUp() {
        
    }

    @After
    public void tearDown() {

    }
    
    @Test(expected = PromotionException.class)
    public void testCreationException() throws PromotionException {
        Threat.create(mockSourceObject);
    }
}
