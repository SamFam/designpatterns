/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threats;

import com.samualmartelli.promoterpattern.interfaces.Promotable;

/**
 * A simple Target object that represents a potential threat registered by a
 * Tracker. 
 *
 * @author Samual Martelli
 */
public class Target implements Promotable {

    private Position currentPosition;
    private final boolean ally;

    /**
     * Creates a target given its position
     *
     * @param initialPosition
     * @param isAllied 
     */
    public Target(Position initialPosition, boolean isAllied) {
        this.currentPosition = initialPosition;
        ally = isAllied;
    }

    public Position getPosition() {
        return currentPosition;
    }
    
    /**
     * Updates the XYZ position of the target
     *
     * @param xCoordinates
     * @param yCoordinates
     * @param zCoordinates
     * @return
     */
    public Target updatePosition(final Integer xCoordinates, final Integer yCoordinates, final Integer zCoordinates) {
        currentPosition.setxCoordinates(xCoordinates);
        currentPosition.setyCoordinates(yCoordinates);
        currentPosition.setzCoordinates(zCoordinates);
        return this;
    }

    public boolean isAlly() {
        return ally;
    }
    
}
