/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threats;

import com.samualmartelli.promoterpattern.interfaces.Promotable;
import com.samualmartelli.promoterpattern.interfaces.Promoter;
import com.samualmartelli.promoterpattern.interfaces.PromotionException;
import com.samualmartelli.promoterpattern.ship.Ship;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Tracker registers a Target and determines if it is a Threat
 *
 * @author Samual Martelli
 */
public class Tracker implements Promoter {

    private final Ship ship;
    private ArrayList<Target> targets;
    private ArrayList<Threat> threats;

    public Tracker() {
        Position zeroPosition = new Position(0,0,0);
        ship = new Ship(zeroPosition);
        targets = new ArrayList<>();
        threats = new ArrayList<>();
    }

    public boolean isDefeated(Threat source) {
        return source.isDefeated();
    }

    public Tracker updateShipCoordinates(final Integer x, final Integer y, final Integer z) {
        ship.updatePosition(x, y, z);
        return this;
    }

    public boolean isHostile(Target source) {
        return !source.isAlly();
    }

    public ArrayList<Target> getTargets() {
        return targets;
    }

    public void addTarget(Target newTarget) {
        targets.add(newTarget);
    }

    public void removeTarget(Target source) {
        targets.remove(source);
    }

    public ArrayList<Threat> getThreats() {
        return threats;
    }

    public void addThreat(Threat newThreat) {
        threats.add(newThreat);
    }

    public void removeThreat(Threat source) {
        threats.remove(source);
    }

    @Override
    public void checkForPromotion(Promotable source) {
        if (source instanceof Target && isHostile((Target) source)) {
            promote(source);
        }
    }

    @Override
    public void promote(Promotable source) {
        if (source instanceof Target) {
            Target previousTarget = (Target) source;
            Threat promotedTarget;
            try {
                promotedTarget = Threat.create(previousTarget);
                this.removeTarget(previousTarget);
                this.addThreat(promotedTarget);
            } catch (PromotionException ex) {
                Logger.getLogger(Tracker.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
