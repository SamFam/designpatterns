/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threats;

import com.samualmartelli.promoterpattern.interfaces.Promotable;
import com.samualmartelli.promoterpattern.interfaces.PromotionException;

/**
 * A Threat is a hostile entity that can be tracked by a Tracker and Jammed 
 * by a jammer.
 * @author Samual Martelli
 */
public class Threat {
    private Position currentPosition;
    private boolean defeated;
    public ThreatPriority priority;

    public enum ThreatPriority {
        SEVERE, MODERATE, LOW, UNKNOWN 
    }
    
    public Threat(Target target) {
        currentPosition = target.getPosition();
        defeated = false;
        priority = ThreatPriority.SEVERE;
        
    }
    
    public Position getPosition() {
        return currentPosition;
    }
    
    /**
     * Updates the XYZ position of the target
     *
     * @param xCoordinates
     * @param yCoordinates
     * @param zCoordinates
     * @return
     */
    public Threat updatePosition(final Integer xCoordinates, final Integer yCoordinates, final Integer zCoordinates) {
        currentPosition.setxCoordinates(xCoordinates);
        currentPosition.setyCoordinates(yCoordinates);
        currentPosition.setzCoordinates(zCoordinates);
        return this;
    }
    
    public boolean isDefeated() {
      return defeated;
    }    
    
    public Threat setDefeated(boolean defeated) {
        this.defeated = defeated;
        return this;
    }
    
    public static Threat create(Promotable sourceObject) throws PromotionException {
        if (sourceObject instanceof Target) {
            return new Threat((Target) sourceObject);
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(PromotionException.PROMOTION_CREATE_FAILURE);
   
            if (sourceObject != null) {
                builder.append(sourceObject.getClass().toString());
            }
            throw new PromotionException(builder.toString());
        }
    }
    
}
