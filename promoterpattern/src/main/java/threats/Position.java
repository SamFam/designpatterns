/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package threats;

/**
 *
 * @author hard-
 */
public class Position {
    private Integer xCoordinates;
    private Integer yCoordinates;
    private Integer zCoordinates;
    
    public Position (Integer x, Integer y , Integer z) {
        xCoordinates = x;
        yCoordinates = y;
        zCoordinates = z;
    }
    
    /**
     * Returns the X Coordinates of a target
     * @return 
     */
    public Integer getxCoordinates() {
        return xCoordinates;
    }

    public Position setxCoordinates(Integer xCoordinates) {
        this.xCoordinates = xCoordinates;
        return this;
    }
    
    public Integer getyCoordinates() {
        return yCoordinates;
    }

    public Position setyCoordinates(Integer yCoordinates) {
        this.yCoordinates = yCoordinates;
        return this;
    }
    
    public Integer getzCoordinates() {
        return zCoordinates;
    }

    public Position setzCoordinates(Integer zCoordinates) {
        this.zCoordinates = zCoordinates;
        return this;
    }
}
