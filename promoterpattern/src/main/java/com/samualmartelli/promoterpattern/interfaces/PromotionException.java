/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.promoterpattern.interfaces;

/**
 * Represents an error with a promotion
 * @author Samual Martelli
 */
public class PromotionException extends Exception{
    
    public final static String PROMOTION_CREATE_FAILURE = "Unable to create PromotedObject of type: ";
    
    public PromotionException(String message) {
        super(message);
    }
}
