/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.promoterpattern.interfaces;

/**
 * A Promotable object can be promoted to a PromotedObject by its corresponding
 * Promoter
 * @author Samual Martelli
 */
public interface Promotable {
    
}
