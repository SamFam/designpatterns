/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.promoterpattern.interfaces;

/**
 * A Promoter object determines if a Promotable object satisfies the criteria 
 * to be promoted to a PromotedObject
 * @author Samual Martelli
 */
public interface Promoter {
    
    public void checkForPromotion(Promotable source);
    
    public void promote(Promotable source);
}
