/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samualmartelli.promoterpattern.ship;

import threats.Position;
import threats.Target;

/**
 * Represents the Ship 
 * @author Samual Martelli
 */
public class Ship extends Target {
    
    public Ship(final Position position) {
        super(position, true);
    }
}
